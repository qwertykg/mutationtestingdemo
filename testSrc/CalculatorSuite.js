const calculator = require("../src/Calculator");

describe("add", function() 
{
    it("Should add 2 numbers", function() 
    {
      let additionResult = calculator.add(34, 6);;
      expect(additionResult).toBe(40);
    });
});

describe("subtract", function() 
{
    it("Should subtract 2 numbers", function() 
    {
      let subtractionResult = calculator.subtract(34, 6);;
      expect(subtractionResult).toBe(28);
    });
});

describe("multiply", function() 
{
    it("Should multiply 2 numbers", function() 
    {
      let muliplicationResult = calculator.multiply(34, 2);;
      expect(muliplicationResult).toBe(68);
    });
});

describe("divide", function() 
{
    it("Should divide 2 numbers", function() 
    {
      let divisionResult = calculator.divide(34, 2);;
      expect(divisionResult).toBe(17);
    });
});

describe("pow", function() 
{
    it("Should raise the first number by the second number", function() 
    {
      let divisionResult = calculator.pow(2, 3);;
      expect(divisionResult).toBe(8);
    });
});

describe("isEven", function() 
{
    it("Should return true if a number is even", function() 
    {
      let isPrime = calculator.isEven(34);;
      expect(isPrime).toBe(true);
    });
});

describe("isOdd", function() 
{
    it("Should return true if a number is odd", function() 
    {
      let isOdd = calculator.isOdd(33);
      expect(isOdd).toBe(true);
    });
});