var Calculator = function () {};

Calculator.add = (number1, number2) => {
        return number1 + number2;
    };

Calculator.subtract = (number1, number2) => {
        return number1 - number2;    
    };

Calculator.multiply = (number1, number2) => {
    return number1 * number2;    
};

Calculator.divide = (number1, number2) => {
    return number1 / number2;    
};

Calculator.pow = (number1, number2) => {
    let result = 1;

    if (number2 === 0) return 1;

    for(let i = 0; i < number2; i++)
    {
        result *= number1
    }
    return result;    
};

Calculator.isEven = (number1) => {
    return number1 % 2 === 0;
};

Calculator.isOdd = (number1) => {
    return number1 % 2 !== 0;
};

module.exports = Calculator;